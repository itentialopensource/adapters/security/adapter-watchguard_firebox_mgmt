# WatchGuard Firebox Management

Vendor: WatchGuard
Homepage: https://www.watchguard.com/

Product: Firebox Management
Product Page: https://www.watchguard.com/wgrd-products/cloud-and-virtual-firewalls/firebox-cloud

## Introduction
We classify Firebox Management into the Security/SASE domain as Firebox Management provides a solution for firewall configuration and management.

## Why Integrate
The Watchguard Firebox Management adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Watchguard Firebox Management in order to manage WatchGuard Fireboxes. With this adapter you have the ability to perform operations on items such as:

- Deployments
- Health

## Additional Product Documentation
[WatchGuard Firebox Management API](https://www.watchguard.com/help/docs/API/Content/en-US/firebox/management/v1/management.html)